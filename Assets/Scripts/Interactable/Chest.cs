﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour {
    private SpriteRenderer spriteRenderer;
    public Sprite openSprite;
    public bool open = false;
    public GameObject content;

    public bool locked = true;

    void Start() {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void Update() {
        if (open) {
            spriteRenderer.sprite = openSprite;
        }
    }

    public string Open() {
        if (!open) {
            locked = false;
            open = true;
            GameObject item = Instantiate(content, new Vector3(transform.position.x, transform.position.y + 0.5f, -5), Quaternion.identity);
            string itemName = item.name;
            Destroy(item, 0.75f);
            return itemName;
        }
        return "";
    }

    public bool IsLocked() {
        return locked;
    }
}
