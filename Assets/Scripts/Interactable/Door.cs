﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    public BoxCollider2D collider;
    public bool locked = false;
    public Sprite lockedSprite;
    public Sprite unlockedSprite;

    void Start() {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        collider = transform.GetChild(0).GetComponent<BoxCollider2D>();
    }

    private void Update() {
        spriteRenderer.sprite = locked ? lockedSprite : unlockedSprite;
    }

    public void Open() {
        locked = false;
        spriteRenderer.enabled = false;
        collider.enabled = false;
    }

    public void Close() {
        spriteRenderer.enabled = true;
        collider.enabled = true;
    }

    public bool IsLocked() {
        return locked;
    }
}
