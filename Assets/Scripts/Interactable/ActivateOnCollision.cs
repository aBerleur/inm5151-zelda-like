﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateOnCollision : MonoBehaviour {

    public List<GameObject> activateObject;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag.Equals("Player")) {
            foreach (var obj in activateObject) {
                IActivable activable = obj.GetComponent<IActivable>();
                if (activable != null) {
                    activable.activate(true);
                }
            }
        }
    }
}
