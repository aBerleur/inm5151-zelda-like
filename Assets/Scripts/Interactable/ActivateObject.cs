﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour, IActivable {

    private SpriteRenderer sprite;
    private BoxCollider2D collider;

    private void Start() {
        sprite = GetComponent<SpriteRenderer>();
        collider = GetComponent<BoxCollider2D>();
    }

    public void activate(bool activate) {
        sprite.enabled = activate;
        collider.enabled = activate;
    }
}
