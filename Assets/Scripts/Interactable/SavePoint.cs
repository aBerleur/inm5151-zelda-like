﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour {

    public bool activated;
    private Animator animator;

    private void Start() {
        animator = GetComponent<Animator>();
    }

    private void Update() {
        animator.SetBool("activate", activated);
    }

    private void OnTriggerStay2D(Collider2D collision) {
        if (collision.tag.Equals("Player") && Input.GetButtonDown("item")) {
            Character character = FindObjectOfType<Character>();
            character.gameObject.GetComponent<Health>().gainHp(20);
            FindObjectOfType<HeartUI>().refreshHp();
            activated = true;
            Save.saveGame();                      
        }
    }
}
