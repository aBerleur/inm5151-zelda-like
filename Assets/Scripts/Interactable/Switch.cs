﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

    public LayerMask hitTarget;
    public Sprite activateSprite;
    public Sprite desactivateSprite;
    public GameObject[] activatables;
    public bool permanent;
    public bool activate = false;

    public void activation() {
        if (!activate) {
            foreach (var obj in activatables) {
                IActivable activable = obj.GetComponent<IActivable>();
                if (activable != null) {
                    activable.activate(false);
                }
            }
            gameObject.GetComponent<SpriteRenderer>().sprite = activateSprite;
        }
        activate = true;
    }

    public void desactivation() {
        if (activate) {
            foreach (var obj in activatables) {
                IActivable activable = obj.GetComponent<IActivable>();
                if (activable != null) {
                    activable.activate(true);
                }
            }
            gameObject.GetComponent<SpriteRenderer>().sprite = desactivateSprite;
        }
        activate = false;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (hitTarget == (hitTarget | (1 << collision.gameObject.layer))) {
            activation();
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (!permanent) {
            if (hitTarget == (hitTarget | (1 << collision.gameObject.layer))) {
                desactivation();
            }
        }
    }
}
