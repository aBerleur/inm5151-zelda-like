﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public AudioSource efxSource;                   
    public AudioSource musicSource;                 
    public static SoundManager instance = null;

    public int masterVolume = 100;
    public int musicVolume = 100;
    public int soundVolume = 100;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Update() {
        efxSource.volume = (masterVolume / 100.0f) * (soundVolume / 100.0f);
        musicSource.volume = (masterVolume / 100.0f) * (musicVolume / 100.0f);
    }

    public void PlaySingle(AudioClip clip) {
        efxSource.clip = clip;
        efxSource.Play();
    }
}
