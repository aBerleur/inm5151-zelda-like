﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IKnockBackable {
    void AddImpact(Vector2 dir, float force);
}
