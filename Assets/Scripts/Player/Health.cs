﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {
    public int maxHp;
    public int currentHp;
    public bool dead;

    public void hit(int dmg) {
        currentHp -= dmg;
        if (currentHp <= 0) {
            if (!dead) {
                die();
            }
        }
    }

    public void gainHp(int hp) {
        currentHp += hp;
        if (currentHp > maxHp) {
            currentHp = maxHp;
        }
    }

    private void die() {
        Bat enemy = GetComponent<Bat>();
        if (enemy)
            enemy.DropItems();
        dead = true;
        Destroy(gameObject);
    }
}
