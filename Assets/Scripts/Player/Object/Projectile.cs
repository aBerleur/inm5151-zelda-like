﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    public int speed;
    public Vector2 direction;
    public LayerMask hitTarget;

    void Update() {
        transform.Translate(direction * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (hitTarget == (hitTarget | (1 << collision.gameObject.layer))) {
            Destroy(gameObject);
        }
    }
}
