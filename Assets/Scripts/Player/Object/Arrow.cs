﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {
    public int speed;
    public Vector2 direction;
    public LayerMask hitTarget;

    void Update() {
        transform.Translate(direction * speed * Time.deltaTime);
        int angle;
        if (direction.Equals(Vector2.up)) {
            angle = -45;
        } else if (direction.Equals(Vector2.right)) {
            angle = -135;
        } else if (direction.Equals(Vector2.left)) {
            angle = 45;
        } else {
            angle = 135;
        }
        transform.GetChild(0).rotation = Quaternion.Euler(0, 0, angle);
        transform.GetChild(0).gameObject.SetActive(true);
    }

    

    private void OnTriggerEnter2D(Collider2D collision) {
        if (hitTarget == (hitTarget | (1 << collision.gameObject.layer))) {
            Destroy(gameObject);
        }
    }
}
