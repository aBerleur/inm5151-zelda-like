﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezingStaff : MonoBehaviour, IObject {

    public GameObject icePrefab;
    public Transform attackLocation;
    public Character character;

    public void useObject() {
        character = FindObjectOfType<Character>();
        if (!character.state.Equals(Character.playerState.Shooting) && !character.state.Equals(Character.playerState.Shooting)) {
            character.state = Character.playerState.Shooting;
            GameObject iceObj = Instantiate(icePrefab);
            attackLocation = character.attackLocation[character.direction];
            iceObj.transform.position = attackLocation.position;
            Ice ice = iceObj.GetComponent<Ice>();
            ice.direction = character.faceDir;
            Invoke("iceBlast", 0.65f);
        }
    }

    private void iceBlast() {
        character.state = Character.playerState.Idle;
    }
}
