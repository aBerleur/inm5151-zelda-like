﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bow : MonoBehaviour, IObject {

    public GameObject arrowPrefab;
    public Transform attackLocation;
    public Character character;
    public bool equipped;
    public Image sprite;

    private void Start() {
        sprite = GetComponent<Image>();
    }

    public void useObject() {
        character = FindObjectOfType<Character>();
        if (!character.state.Equals(Character.playerState.Shooting) && !character.state.Equals(Character.playerState.Shooting) && equipped) {
            character.state = Character.playerState.Shooting;
            Invoke("shootArrow", 0.65f);
        }
    }

    private void shootArrow() {
        GameObject arrowObj = Instantiate(arrowPrefab);
        attackLocation = character.attackLocation[character.direction];
        arrowObj.transform.position = attackLocation.position;
        Arrow arrow = arrowObj.GetComponent<Arrow>();
        arrow.direction = character.faceDir;
        character.state = Character.playerState.Idle;
    }

    private void Update() {
        sprite.enabled = equipped;
    }
}
