﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ice : MonoBehaviour {

    public LayerMask hitTarget;
    public GameObject iceBlockPrefab;
    public Vector2 direction;
    bool triggered;

    void Start() {
        Destroy(gameObject, transform.GetChild(0).GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length);
    }

    private void Update() {
        int angle;
        if (direction.Equals(Vector2.up)) {
            angle = 0;
        } else if (direction.Equals(Vector2.right)) {
            angle = 270;
        } else if (direction.Equals(Vector2.left)) {
            angle = 90;
        } else {
            angle = 180;
        }
        transform.GetChild(0).rotation = Quaternion.Euler(0, 0, angle);
        transform.GetChild(0).gameObject.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (hitTarget == (hitTarget | (1 << collision.gameObject.layer))) {
            print(collision.tag);
            Transform position = collision.transform;
            Destroy(collision.gameObject);
            GameObject iceblock = Instantiate(iceBlockPrefab);
            iceblock.transform.position = position.position;
        }
    }
}
