﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireAmulet : MonoBehaviour, IObject {
    
    public CircleShot circleShot;
    public Character character;

    private void Start() {
        circleShot = GetComponent<CircleShot>();
    }

    public void useObject() {
        character = FindObjectOfType<Character>();
        if (!character.state.Equals(Character.playerState.Shooting) && !character.state.Equals(Character.playerState.Shooting)) {
            character.state = Character.playerState.Shooting;
            Invoke("ringOfFire", 0.65f);
        }
    }

    private void ringOfFire() {
        circleShot.shoot();
        character.state = Character.playerState.Idle;
    }
}
