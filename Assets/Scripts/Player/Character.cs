﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour, IKnockBackable {

    public enum playerState {
        Idle,
        Moving,
        Attacking,
        Shooting
    }

    [SerializeField]
    private int moveSpeed;
    private Controller2DCharacter controller;
    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public playerState state;

    private Transform attackHit;
    public Transform[] attackLocation;
    
    public bool isPushingX = false;
    public bool isPushingY = false;
    public Controller2D pushableItem;

    public float invulnerabilityTime;
    public bool hitable = true;

    public Vector2 faceDir;
    public int direction = 2; // 0 = Up, 1 = Right, 2 = Down, 3 = Left
    private static Vector2[] possibleDir = { Vector2.up, Vector2.right, Vector2.down, Vector2.left };

    private Collider2D objectInCollision;

    private Vector2 impact = Vector3.zero;
    private float mass = 3;
    public IObject objectInUse;

    public Text keyNumbersText;
    public Text coinNumbersText;
    public int keyNumbers = 1;
    public int coinNumbers = 0;
    

    void Start() {
        controller = GetComponent<Controller2DCharacter>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        attackHit = transform.GetChild(0);       
        if (PlayerPrefs.HasKey(Save.currentSave + "CharacterPosX")) {
            Save.loadGame();            
        }
        UpdateText();
    }   

    void Update() {
        Vector2 input;
        Vector2 velocity;
        faceDir = possibleDir[direction];

        if (direction == 1 || direction == 3) {
            attackHit.rotation = Quaternion.Euler(0, 0, 90);
        } else {
            attackHit.rotation = Quaternion.Euler(0, 0, 0);
        }

        attackHit.position = attackLocation[direction].position;

        if (Input.GetKeyDown(KeyCode.Y)) {
            StartCoroutine("invulnerability");
        }
        if (Input.GetButtonDown("attack") && !state.Equals(playerState.Attacking) && !state.Equals(playerState.Shooting)) {
            StartCoroutine("attack");
        }

        if (Input.GetButtonDown("Use") && !GameManager.instance.isPaused) {
            objectInUse.useObject();
        }
        if (impact.magnitude > 0.3) {
            controller.Move(impact * Time.deltaTime);
        }   
        //movement
        if (!GameManager.instance.isPaused) {
            input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            if (!state.Equals(playerState.Attacking) && !state.Equals(playerState.Shooting)) {
                velocity.x = input.x * moveSpeed;
                velocity.y = input.y * moveSpeed;                

                if (!Input.GetButton("item") || isPushingX && input.y != 0 || isPushingY && input.x != 0 ||
                        isPushingX && Mathf.Abs(transform.position.x - pushableItem.transform.position.x) > 1.2f ||
                        isPushingY && Mathf.Abs(transform.position.y - pushableItem.transform.position.y) > 1.2f) {
                    isPushingX = false;
                    isPushingY = false;
                    pushableItem = null;
                }
                else if (pushableItem) {
                    pushableItem.Move(velocity * Time.deltaTime);
                }

                if (!(impact.magnitude > 0.3)) {                    
                    controller.Move(velocity * Time.deltaTime);
                }
                
                //States
                if (input.x == 0 && input.y == 0) {
                    state = playerState.Idle;
                } else {
                    state = playerState.Moving;
                    if (input.x > 0)
                        direction = 1;
                    else if (input.x < 0)
                        direction = 3;

                    if (input.y > 0)
                        direction = 0;
                    else if (input.y < 0)
                        direction = 2;
                }
            }
            //Animation
            animator.SetBool("isMoving", state.Equals(playerState.Moving));
            animator.SetBool("isAttacking", state.Equals(playerState.Attacking));
            animator.SetInteger("direction", direction);
            spriteRenderer.flipX = direction == 3 ? true : false;

            //Interaction
            if (Input.GetButton("item")) {
                if (objectInCollision != null) {
                    if (objectInCollision.tag.Equals("Chest")) {
                        Chest chest = objectInCollision.GetComponent<Chest>();
                        string itemName = "";
                        if (chest.IsLocked()) {
                            if (keyNumbers > 0) {
                                keyNumbers--;
                                UpdateText();
                                itemName = chest.Open();
                                equipItem(itemName);
                            }
                        } else {
                            itemName = chest.Open();
                            equipItem(itemName);
                        }
                        
                    }
                }
            }
        }
        impact = Vector3.Lerp(impact, Vector3.zero, 10 * Time.deltaTime);
    }

    private void UpdateText() {
        keyNumbersText.text = "x " + keyNumbers;
        coinNumbersText.text = "x " + coinNumbers;
    }

    public void equipItem(string itemName) {
        if (itemName.Equals("ItemBow(Clone)")) {
            FindObjectOfType<Bow>().equipped = true;
        } else if (itemName.Equals("ItemKey(Clone)")) {
            keyNumbers++;
        } else if (itemName.Equals("ItemCoin(Clone)")) {
            coinNumbers += 10;
        }
        UpdateText();
    }

    public void AddImpact(Vector2 dir, float force) {
        dir.Normalize();
        impact += dir.normalized * force / mass;
    }

    IEnumerator attack() {
        state = playerState.Attacking;
        yield return new WaitForSeconds(0.65f);        
        state = playerState.Idle;
    }

    IEnumerator invulnerability() {
        hitable = false;
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.Sleep();
        for (int i = 0; i < 5; i++) {
            GetComponent<SpriteRenderer>().color = Color.black;
            yield return new WaitForSeconds(invulnerabilityTime/10);
            GetComponent<SpriteRenderer>().color = Color.white;
            yield return new WaitForSeconds(invulnerabilityTime / 10);
        } 
        rb.WakeUp();
        hitable = true;
    }

    private void OnTriggerStay2D(Collider2D other) {
        if (other.tag.Equals("Chest")) {
            objectInCollision = other;
        } else if (other.tag.Equals("Door")) {
            Door door = other.gameObject.GetComponent<Door>();
            if (door.IsLocked() ) {
                if (keyNumbers > 0 && Input.GetButton("item")) {
                    keyNumbers--;
                    UpdateText();
                    door.Open();
                }
            } else {
                door.Open();
            }
        } else if (other.tag.Equals("Key")) {
            keyNumbers++;
            UpdateText();
            Destroy(other.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag.Equals("Chest")) {
            objectInCollision = null;
        } else if (other.tag.Equals("Door")) {
            other.gameObject.GetComponent<Door>().Close();
        }
    }
}
