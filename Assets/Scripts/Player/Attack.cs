﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {
    public int dmg;
    public int knockbackForce;
    public LayerMask hitTarget;

    private void OnTriggerEnter2D(Collider2D other) {
        if (hitTarget == (hitTarget | (1 << other.gameObject.layer)) && 
                (other.tag.Equals("Player") && other.GetComponent<Character>().hitable || other.tag.Equals("Enemy"))) {
            other.GetComponent<Health>().hit(dmg);
            Vector2 direction = ((Vector2)other.transform.position - (Vector2)transform.position);
            IKnockBackable knockbackable = other.GetComponent<IKnockBackable>();
            if (knockbackable != null) {
                knockbackable.AddImpact(direction, knockbackForce);
            }

            if (other.tag.Equals("Player")) {
                other.GetComponent<Character>().StartCoroutine("invulnerability");
                FindObjectOfType<HeartUI>().setHp();
            } 
        }
    }
}
