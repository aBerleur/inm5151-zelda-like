﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

    public Canvas inventory;
    public Image objectImage;
    public InventorySelector selector;
    private Character character;
    

    private void Start() {
        character = FindObjectOfType<Character>();
        character.objectInUse = objectImage.GetComponent<IObject>();
    }

    private void Update() {        
        if (Input.GetButtonDown("Inventory")) {
            activateInventory();
        }
        if (selector.selectedObject != null) {
            character.objectInUse = selector.selectedObject.GetComponent<IObject>();
        }
    }

    public void activateInventory() {
        if (inventory.enabled) {
            if (selector.selectedTile.GetChild(0).GetComponent<Image>().enabled) {
                changeImage();
            } else {
                objectImage.color = new Color(objectImage.color.r, objectImage.color.g, objectImage.color.b, 0);
            }
            
        }
        inventory.enabled = !inventory.enabled;
        Time.timeScale = Time.timeScale.Equals(0f) ? 1f : 0f;
        GameManager.instance.isPaused = inventory.enabled;
    }

    public void changeImage() {
        if (selector.selectedObject.enabled) {
            objectImage.sprite = selector.selectedObject.sprite;
            objectImage.color = new Color(objectImage.color.r, objectImage.color.g, objectImage.color.b, 1f);
        } else {
            objectImage.color = new Color(objectImage.color.r, objectImage.color.g, objectImage.color.b, 0);
        }
    }
}
