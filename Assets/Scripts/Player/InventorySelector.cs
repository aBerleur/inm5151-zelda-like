﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySelector : MonoBehaviour {

    public Transform grid;
    public Transform selectedTile;
    public Inventory inventory;
    public Image selectedObject;
    public Sprite emptyObject;
    public Vector2 size;
    public Vector2 pos;
    private bool axisXInUse;
    private bool axisYInUse;
    

    void Update() {
        if (inventory.inventory.enabled) {
            if (Input.GetAxisRaw("Vertical") != 0) {
                if (axisYInUse == false) {
                    if (Input.GetAxisRaw("Vertical") < 0) {
                        pos.y++;
                        if (pos.y > size.y - 1) {
                            pos.y = size.y - 1;
                        }
                    } else if (Input.GetAxisRaw("Vertical") > 0) {
                        pos.y--;
                        if (pos.y < 0) {
                            pos.y = 0;
                        }
                    }
                    axisYInUse = true;
                }
            }
            if (Input.GetAxisRaw("Vertical") == 0) {
                axisYInUse = false;
            }

            if (Input.GetAxisRaw("Horizontal") != 0) {
                if (axisXInUse == false) {
                    if (Input.GetAxisRaw("Horizontal") > 0) {
                        pos.x++;
                        if (pos.x > size.x - 1) {
                            pos.x = size.x - 1;
                        }
                    } else if (Input.GetAxisRaw("Horizontal") < 0) {
                        pos.x--;
                        if (pos.x < 0) {
                            pos.x = 0;
                        }
                    }
                    axisXInUse = true;
                }
            }
            if (Input.GetAxisRaw("Horizontal") == 0) {
                axisXInUse = false;
            }
            selectObject();
        }
    }

    public void selectObject() {
        selectedTile = grid.GetChild((int)pos.x + (int)pos.y * (int)size.x);
        transform.position = selectedTile.position;
        transform.parent = selectedTile;
        selectedObject = selectedTile.GetChild(0).GetComponent<Image>();
        inventory.changeImage();
    }
}
