﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartUI : MonoBehaviour {
    
    Health health;
    
    void Start() {
        refreshHp();
    }

    public void setHp() {
        int diff = health.maxHp - health.currentHp;
        for (int i = health.maxHp; i > health.currentHp; i--) {
            transform.GetChild(i-1).GetChild(1).gameObject.SetActive(false);
        }
    }

    public void refreshHp() {
        health = FindObjectOfType<Character>().GetComponent<Health>();
        for (int i = 0; i < health.maxHp; i++) {
            transform.GetChild(i).gameObject.SetActive(true);
        }

        for (int i = 0; i < health.maxHp; i++) {
            transform.GetChild(i).GetChild(1).gameObject.SetActive(true);
        }

        int diff = health.maxHp - health.currentHp;
        for (int i = health.maxHp; i > health.currentHp; i--) {
            transform.GetChild(i - 1).GetChild(1).gameObject.SetActive(false);
        }
    }
}
