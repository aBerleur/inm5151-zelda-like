﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour
{
    private bool axisInUse;
    public GameObject master;
    public GameObject music;
    public GameObject sound;
    public Image fullBar;
    public Image emptyBar;
    private string volumeType;
    private Selector selector;

    private void Start() {
        selector = GetComponent<Selector>();
        changeVolumeSprite(master, SoundManager.instance.masterVolume);
        changeVolumeSprite(music, SoundManager.instance.musicVolume);
        changeVolumeSprite(sound, SoundManager.instance.soundVolume);
    }

    void Update() {
        if (Input.GetAxisRaw("Horizontal") != 0) {
            if (axisInUse == false) {
                if (Input.GetAxisRaw("Horizontal") < 0) {
                    reduceVolume(true);
                } else if (Input.GetAxisRaw("Horizontal") > 0) {
                    reduceVolume(false);
                }
                axisInUse = true;
            }
        }
        if (Input.GetAxisRaw("Horizontal") == 0) {
            axisInUse = false;
        }
        volumeType = selector.position[selector.currentPos].name;
    }

    void reduceVolume(bool reduced) {
        switch (volumeType) {
            case "Master":
                SoundManager.instance.masterVolume += reduced ? -10 : 10;
                SoundManager.instance.masterVolume = SoundManager.instance.masterVolume > 100 ? 100 : SoundManager.instance.masterVolume < 0 ? 0 : SoundManager.instance.masterVolume;
                changeVolumeSprite(master, SoundManager.instance.masterVolume);
                break;
            case "Music":
                SoundManager.instance.musicVolume += reduced ? -10 : 10;
                SoundManager.instance.musicVolume = SoundManager.instance.musicVolume > 100 ? 100 : SoundManager.instance.musicVolume < 0 ? 0 : SoundManager.instance.musicVolume;
                changeVolumeSprite(music, SoundManager.instance.musicVolume);
                break;
            case "Sound":
                SoundManager.instance.soundVolume += reduced ? -10 : 10;
                SoundManager.instance.soundVolume = SoundManager.instance.soundVolume > 100 ? 100 : SoundManager.instance.soundVolume < 0 ? 0 : SoundManager.instance.soundVolume;
                changeVolumeSprite(sound, SoundManager.instance.soundVolume);
                break;
        }
        Save.saveOptions();
    }

    void changeVolumeSprite(GameObject volume, int sound) {
        int i = sound;
        foreach (Transform child in volume.transform) {
            if (i > 0) {
                child.GetComponent<Image>().sprite = fullBar.sprite;
            } else {
                child.GetComponent<Image>().sprite = emptyBar.sprite;
            }
            i -= 10;
        }
    }
}
