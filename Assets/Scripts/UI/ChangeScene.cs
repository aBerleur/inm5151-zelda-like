﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour, IAction {

    public new string name;

    public void executeAction() {
        SceneManager.LoadScene(name);
        GameManager.instance.isPaused = false;
    }
}
