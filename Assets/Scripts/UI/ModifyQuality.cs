﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyQuality : MonoBehaviour, IAction {

    public int quality;

    public void executeAction() {
        QualitySettings.SetQualityLevel(quality, true);
    }
}
