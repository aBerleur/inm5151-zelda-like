﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenMenu : MonoBehaviour {

    public GameObject obj;
    public GameObject startOptions;

    void Update() {
        if (Input.GetButtonDown("back")) {
            obj.SetActive(!obj.activeSelf);
            GameManager.instance.isPaused = obj.activeSelf ? true : false;
            if (!GameManager.instance.isPaused) {
                foreach (Transform child in obj.transform) {
                    if (!child.gameObject.name.Equals("Background")) {
                        child.gameObject.SetActive(false);
                    }
                }
                startOptions.SetActive(true);
            }
        }
    }
}
