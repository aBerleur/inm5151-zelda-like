﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifiyFullScreen : MonoBehaviour, IAction {

    public bool fullscreen;

    public void executeAction() {
        GameManager.instance.isFullScreen = fullscreen;
        Screen.SetResolution(GameManager.instance.width, GameManager.instance.height, GameManager.instance.isFullScreen);
        Save.saveOptions();
    }
}
