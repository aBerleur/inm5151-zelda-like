﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour {

    public AudioClip moveSound;
    public List<Image> position;
    public int currentPos = 0;
    private Image selectorImage;
    private bool axisInUse;
    [SerializeField]
    //the selector will not reset position if isResetable is false
    private bool isResetable;

    private void Start() {
        selectorImage = GetComponent<Image>();
        gameObject.transform.position = position[currentPos].transform.position;
    }

    void Update () {

        gameObject.transform.position = position[currentPos].transform.position;
        selectorImage.rectTransform.sizeDelta = new Vector2(position[currentPos].sprite.rect.width + selectorImage.sprite.rect.width * 2, selectorImage.sprite.rect.height);

        if (Input.GetAxisRaw("Vertical") != 0) {
            if (axisInUse == false) {
                if (Input.GetAxisRaw("Vertical") < 0) {
                    currentPos++;
                    if (currentPos > position.Count - 1) {
                        currentPos = 0;
                    }
                } else if (Input.GetAxisRaw("Vertical") > 0) {
                    currentPos--;
                    if (currentPos < 0) {
                        currentPos = position.Count - 1;
                    }
                }
                axisInUse = true;
                SoundManager.instance.PlaySingle(moveSound);
            }
        }
        if (Input.GetAxisRaw("Vertical") == 0) {
            axisInUse = false;
        }
        if (Input.GetButtonDown("attack")) {
            IAction [] actions = position[currentPos].gameObject.GetComponents<IAction>();
            foreach (var action in actions) {
                action.executeAction();
            }
        }
	}

    void OnDisable() {
        if (isResetable) {
            currentPos = 0;
            gameObject.transform.position = position[currentPos].transform.position;
            selectorImage.rectTransform.sizeDelta = new Vector2(position[currentPos].sprite.rect.width + selectorImage.sprite.rect.width * 2, selectorImage.sprite.rect.height);
        }
    }
}
