﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMenu : MonoBehaviour, IAction {

    public GameObject currentMenu;
    public GameObject newMenu;
    
    public void executeAction() {
        currentMenu.SetActive(false);
        newMenu.SetActive(true);
    }
}
