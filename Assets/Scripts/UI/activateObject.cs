﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class activateObject : MonoBehaviour, IAction {

    public GameObject obj;

    public void executeAction() {
        obj.SetActive(false);
        GameManager.instance.isPaused = false;
    }
}
