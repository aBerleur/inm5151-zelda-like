﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyResolution : MonoBehaviour, IAction {

    public int width;
    public int height;

    public void executeAction() {
        GameManager.instance.width = width;
        GameManager.instance.height = height;
        Screen.SetResolution(GameManager.instance.width, GameManager.instance.height, GameManager.instance.isFullScreen);
        Save.saveOptions();
    }
}
