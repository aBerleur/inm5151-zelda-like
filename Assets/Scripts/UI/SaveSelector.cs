﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSelector : MonoBehaviour, IAction {

    public int saveSlot;

    public void executeAction() {
        Save.currentSave = "save" + saveSlot;
    }

    private void Update() {
        if (PlayerPrefs.HasKey("save" + saveSlot + "CharacterPosX")) {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }
}
