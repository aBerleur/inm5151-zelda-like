﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Save : MonoBehaviour {

    public static string currentSave;

    public static void saveGame() {

        Character character = FindObjectOfType<Character>();
        PlayerPrefs.SetFloat(currentSave + "CharacterPosX", character.transform.position.x);
        PlayerPrefs.SetFloat(currentSave + "CharacterPosY", character.transform.position.y);
        PlayerPrefs.SetInt(currentSave + "CharacterKey", character.keyNumbers);
        PlayerPrefs.SetInt(currentSave + "CharacterCoin", character.coinNumbers);

        InventorySelector selector = FindObjectOfType<InventorySelector>();
        PlayerPrefs.SetFloat(currentSave + "ItemInUseX", selector.pos.x);
        PlayerPrefs.SetFloat(currentSave + "ItemInUseY", selector.pos.y);

        SavePoint savePoint = FindObjectOfType<SavePoint>();
        PlayerPrefs.SetInt(currentSave + "SavePoint", savePoint.activated ? 1 : 0);

        Bow bow = FindObjectOfType<Bow>();
        PlayerPrefs.SetInt(currentSave + "BowEquipped", bow.equipped ? 1 : 0);

        GameObject block = GameObject.FindGameObjectWithTag("Pushable");
        PlayerPrefs.SetFloat(currentSave + "BlockPosX", block.transform.position.x);
        PlayerPrefs.SetFloat(currentSave + "BlockPosY", block.transform.position.y);

        GameObject key = GameObject.FindGameObjectWithTag("Key");
        PlayerPrefs.SetInt(currentSave + "KeyDrop", key == null ? 0 : 1);

        Lich lich = FindObjectOfType<Lich>();
        PlayerPrefs.SetInt(currentSave + "Lich", lich == null ? 0 : 1);

        Door[] doors = FindObjectsOfType<Door>();
        for (int i = 0; i < doors.Length; i++) {
            PlayerPrefs.SetInt(currentSave + "Door" + doors[i].gameObject.transform.position.sqrMagnitude, doors[i].locked ? 1 : 0);
        }

        Switch[] switches = FindObjectsOfType<Switch>();
        for (int i = 0; i < switches.Length; i++) {
            PlayerPrefs.SetInt(currentSave + "Switch" + switches[i].gameObject.transform.position.sqrMagnitude, switches[i].activate ? 1 : 0);
        }

        Chest[] chests = FindObjectsOfType<Chest>();
        for (int i = 0; i < chests.Length; i++) {
            PlayerPrefs.SetInt(currentSave + "ChestOpen" + chests[i].gameObject.transform.position.sqrMagnitude, chests[i].open ? 1 : 0);
            PlayerPrefs.SetInt(currentSave + "ChestLocked" + chests[i].gameObject.transform.position.sqrMagnitude, chests[i].locked ? 1 : 0);
        }
    }

    public static void loadGame() {
        Character character = FindObjectOfType<Character>();
        character.transform.position = new Vector3(PlayerPrefs.GetFloat(currentSave + "CharacterPosX"), 
                                        PlayerPrefs.GetFloat(currentSave + "CharacterPosY"), character.transform.position.z);
        character.keyNumbers = PlayerPrefs.GetInt(currentSave + "CharacterKey");
        character.coinNumbers = PlayerPrefs.GetInt(currentSave + "CharacterCoin");

        GameObject block = GameObject.FindGameObjectWithTag("Pushable");
        block.transform.position = new Vector3(PlayerPrefs.GetFloat(currentSave + "BlockPosX"),
                                        PlayerPrefs.GetFloat(currentSave + "BlockPosY"), character.transform.position.z);

        FindObjectOfType<Bow>().equipped = PlayerPrefs.GetInt(currentSave + "BowEquipped") == 1;
        FindObjectOfType<SavePoint>().activated = PlayerPrefs.GetInt(currentSave + "SavePoint") == 1;

        if (PlayerPrefs.GetInt(currentSave + "KeyDrop") == 0) {
            Destroy(GameObject.FindGameObjectWithTag("Key"));
        }

        if (PlayerPrefs.GetInt(currentSave + "Lich") == 0) {
            Destroy(FindObjectOfType<Lich>().gameObject);
        }

        InventorySelector selector = FindObjectOfType<InventorySelector>();
        selector.pos.x = PlayerPrefs.GetFloat(currentSave + "ItemInUseX");
        selector.pos.y = PlayerPrefs.GetFloat(currentSave + "ItemInUseY");
        selector.selectObject();
        selector.inventory.changeImage();

        Door[] doors = FindObjectsOfType<Door>();
        for (int i = 0; i < doors.Length; i++) {
            doors[i].locked = PlayerPrefs.GetInt(currentSave + "Door" + doors[i].gameObject.transform.position.sqrMagnitude) == 1;
        }

        Switch[] switches = FindObjectsOfType<Switch>();
        for (int i = 0; i < switches.Length; i++) {
            if (PlayerPrefs.GetInt(currentSave + "Switch" + switches[i].gameObject.transform.position.sqrMagnitude) == 1) {
                switches[i].activation();
            }
        }

        Chest[] chests = FindObjectsOfType<Chest>();
        for (int i = 0; i < chests.Length; i++) {
            chests[i].open = PlayerPrefs.GetInt(currentSave + "ChestOpen" + chests[i].gameObject.transform.position.sqrMagnitude) == 1;
            chests[i].locked = PlayerPrefs.GetInt(currentSave + "ChestLocked" + chests[i].gameObject.transform.position.sqrMagnitude) == 1;
        }
    }

    public static void saveOptions() {
        PlayerPrefs.SetInt("MasterVolume", SoundManager.instance.masterVolume);
        PlayerPrefs.SetInt("SoundVolume", SoundManager.instance.soundVolume);
        PlayerPrefs.SetInt("MusicVolume", SoundManager.instance.musicVolume);

        PlayerPrefs.SetInt("HeightResolution", GameManager.instance.height);
        PlayerPrefs.SetInt("WidthResolution", GameManager.instance.width);
        PlayerPrefs.SetInt("FullScreen", GameManager.instance.isFullScreen ? 1 : 0);
    }

    public static void loadOptions() {
        SoundManager.instance.masterVolume = PlayerPrefs.GetInt("MasterVolume");
        SoundManager.instance.soundVolume = PlayerPrefs.GetInt("SoundVolume");
        SoundManager.instance.musicVolume = PlayerPrefs.GetInt("MusicVolume");
        GameManager.instance.height = PlayerPrefs.GetInt("HeightResolution");
        GameManager.instance.width = PlayerPrefs.GetInt("WidthResolution");
        GameManager.instance.isFullScreen = PlayerPrefs.GetInt("FullScreen") == 1;

        Screen.SetResolution(PlayerPrefs.GetInt("WidthResolution"), PlayerPrefs.GetInt("HeightResolution"), PlayerPrefs.GetInt("FullScreen") == 1);
    }
}
