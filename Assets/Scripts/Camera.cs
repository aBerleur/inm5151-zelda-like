﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {
    private Transform pos;

    private void Start() {
        pos = FindObjectOfType<Character>().transform;
    }
    void Update() {
        if (pos != null) {
            transform.position = new Vector3(pos.position.x, pos.position.y, transform.position.z);
        }
    }
}
