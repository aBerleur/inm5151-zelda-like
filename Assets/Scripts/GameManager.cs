﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool isFullScreen;
    public bool isPaused;
    public int width;
    public int height;
    public static GameManager instance = null;

    void Awake() {
        if (instance == null) {
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start() {
        if (PlayerPrefs.HasKey("MasterVolume")) {
            Save.loadOptions();
        }
    }

    public void Update() {
        if (isPaused) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
    }
}
