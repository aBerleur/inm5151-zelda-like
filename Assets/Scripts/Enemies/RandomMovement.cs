﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour, IKnockBackable {

    private Controller2D controller;
    public float speed;
    public Vector2 dir;
    private float mass = 3;
    private Vector2 impact = Vector3.zero;
    private SpriteRenderer sprite;

    void Start() {
        controller = GetComponent<Controller2D>();
        sprite = GetComponent<SpriteRenderer>();
    }
    
    void Update() {
        if (impact.magnitude > 0.3) {
            controller.Move(impact * Time.deltaTime);
        } else {
            controller.Move(dir.normalized * speed * Time.deltaTime);
        }
        impact = Vector3.Lerp(impact, Vector3.zero, 10 * Time.deltaTime);
        sprite.flipX = dir.x > 0.7f;
    }

    public void AddImpact(Vector2 dir, float force) {
        dir.Normalize();
        impact += dir.normalized * force / mass;        
    }

    public void changeDir(Vector2 newDir) {
        if (newDir.x == 0 || newDir.y == 0) {
            dir = new Vector2(newDir.x, newDir.y);
        } else {
            dir = new Vector2(newDir.x + Random.Range(0, 0.3f), newDir.y + Random.Range(0, 0.3f));
        }
    }
}