﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum bossState {
    Engage,
    Sleep
}

public class Lich : MonoBehaviour, IActivable {

    private Controller2D controller;
    public float speed;
    public Vector2 dir;
    private SpriteRenderer sprite;
    private Health health;
    private bossState state = bossState.Sleep;
    public Slider slider;

    void Start() {
        controller = GetComponent<Controller2D>();
        health = GetComponent<Health>();
        sprite = GetComponent<SpriteRenderer>();
    }

    public void Update() {
        if (state.Equals(bossState.Engage)) {
            slider.value = health.currentHp;
        }
    }

    public void changeDir(Vector2 newDir) {
        if (newDir.x == 0 || newDir.y == 0) {
            dir = new Vector2(newDir.x, newDir.y);
        } else {
            dir = new Vector2(newDir.x + Random.Range(0, 0.3f), newDir.y + Random.Range(0, 0.3f));
        }
    }

    private void OnDestroy() {
        if (slider) {
            slider.gameObject.SetActive(false);
        }
    }

    public void activate(bool activate) {
        state = activate ? bossState.Engage : bossState.Sleep;
        slider.gameObject.SetActive(activate);
    }
}
