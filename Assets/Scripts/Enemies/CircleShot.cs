﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleShot : MonoBehaviour {

    public GameObject projectile;
    public GameObject source;

    public void shoot() {
        for (int i = 0; i < 8; i++) {
            GameObject fireObj = Instantiate(projectile);
            fireObj.transform.position = new Vector3(source.transform.position.x, source.transform.position.y,-2);
            Projectile fire = fireObj.GetComponent<Projectile>();
            fire.enabled = true;
            fire.direction = new Vector2(Mathf.Cos(i * 45 * Mathf.Deg2Rad), Mathf.Sin(i * 45 * Mathf.Deg2Rad));
        }
    }
}
