﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enemyState {
    Patrol,
    Attack
}

public class Bat : MonoBehaviour, IKnockBackable {

    public int speed;
    private Animator animator;
    private int direction = 2; // 0 = Up, 1 = Right, 2 = Down, 3 = Left
    private Vector2 velocity;
    private Patrol patrol;
    public enemyState state;
    private Controller2D controller;
    private Transform target;
    private Vector2 impact = Vector3.zero;
    private float mass = 3;
    public GameObject drop;

    void Start() {
        controller = GetComponent<Controller2D>();
        animator = GetComponent<Animator>();
        patrol = GetComponent<Patrol>();
        target = FindObjectOfType<Character>().transform;
    }

    void Update() {
        if (state.Equals(enemyState.Patrol)) {
            velocity = (patrol.destination - (Vector2)transform.position).normalized * speed;
        } else if (state.Equals(enemyState.Attack) && target != null) {
            velocity = ((Vector2)target.position - (Vector2)transform.position).normalized * speed;
        }
        if (velocity.x == 0 && velocity.y == 0) {
            direction = 2;
        } else {
            if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y)) {
                direction = velocity.x > 0 ? 1 : 3;
            } else {
                direction = velocity.y > 0 ? 0 : 2;
            }
        }

        if (impact.magnitude > 0.3) {
            controller.Move(impact * Time.deltaTime);
        } else {
            controller.Move(velocity * Time.deltaTime);
        }
        impact = Vector3.Lerp(impact, Vector3.zero, 10 * Time.deltaTime);
        animator.SetInteger("dir", direction);
    }

    public void AddImpact(Vector2 dir, float force) {
        dir.Normalize();
        impact += dir.normalized * force / mass;
    }

    public void DropItems() {
        Instantiate(drop, transform.position, Quaternion.identity);
    }
}
