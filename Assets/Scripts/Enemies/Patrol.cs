﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrol : MonoBehaviour {

    public Transform[] points;
    private int destPoint = 0;
    public Vector2 destination;

    void Start() {
        GotoNextPoint();
    }

    void GotoNextPoint() {
        if (points.Length == 0) {
            return;
        }
        destination = points[destPoint].position;
        destPoint = (destPoint + 1) % points.Length;
    }


    void Update() {
        if (Vector3.Distance(transform.position,destination) < 0.5f) {
            GotoNextPoint();
        }
    }
}
