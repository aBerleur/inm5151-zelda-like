﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggroDetection : MonoBehaviour {

    private CircleCollider2D detection;
    private Bat enemy;

    private void Start() {
        enemy = GetComponentInParent<Bat>();
        detection = GetComponent<CircleCollider2D>();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag.Equals("Player")) {
            enemy.state = enemyState.Attack;
            detection.radius = 8;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (other.tag.Equals("Player")) {
            enemy.state = enemyState.Patrol;
            detection.radius = 5;
        }
    }
}
