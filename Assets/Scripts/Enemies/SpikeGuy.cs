﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeGuy : MonoBehaviour {

    public CircleShot circleShot;
    public RandomMovement movement;
    private float timer;
    public int shootingDelay;

    void Start() {
        circleShot = GetComponent<CircleShot>();
        movement = GetComponent<RandomMovement>();
    }

    private void Update() {
        timer++;
        if (timer > shootingDelay * 60) {
            StartCoroutine(shoot());
            timer = 0;
        }
    }

    IEnumerator shoot() {
        float speed = movement.speed;
        movement.speed = 0;
        yield return new WaitForSeconds(1f);
        circleShot.shoot();
        movement.speed = speed;
    }
}
