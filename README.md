# Saint Knight

![](demo.gif)

## Description

Saint knight est un prototype de jeu 2D fait avec Unity. C'est un jeu d'action-aventure inspiré de Zelda: A link to the past. Il a été réaliser dans le cadre du cours : INM5151 - Projet d'analyse et de modélisation.

Le jeu est jouable en lançant l'exécutable suivant : 
``build/INM5151-Zelda-like prototype.exe``

## Auteurs
- Anthony Berleur
- Étienne Paquette Perrier
- Audrey-Ann Raymond


## Contenu des sprints

Sprint 1 : Jouer et gérer les configurations
- Jouer
    - Se déplacer
    - Attaquer
    - Pousser un bloc
    - Ouvrir une porte
    - Ouvrir un coffre
    - Commencer la partie
    - Quitter la partie
    - Quitter le jeu
- Gérer les configurations
    - Configurer les options audio
        - Configurer le master volume
        - Configurer le music volume
        - Configurer le sound volume
    - Configurer les options vidéo
        - Configurer le mode d’écran
        - Configurer la résolution d’écran
        - Configurer la qualité
    - Configurer les touches claviers
        - Configurer les touches de d´ eplacements
        - Configurer la touche d’interaction
2. Sprint 2 : Gérer l’inventaire
    - Gérer l’inventaire
        - Changer d’objets
        - Ramasser un objet
        - Déverrouiller un coffre
        - Déverrouiller une porte
3. Sprint 3 : Sauvegarder et charger une partie
    - Sauvegarder une partie
    - Charger une partie

## Dependency

You need [Unity](https://unity.com/) to open the project
